#!/usr/bin/python

from Tkinter import *

class App():
            
    def getSpeed(self):
        # TODO Create the speed interface
        speed = 55
        return  str(speed);
    
    def getTach(self):
        # TODO Create the tach interface
        tach = 5225
        return  str(tach);
    
    def getWaterTemp(self):
        # TODO Create the water interface
        water = 108
        return  str(water);
    
    def getBoost(self):
        # TODO Create the boost interface
        psi = 15
        return  str(psi);
    
    def __init__(self):
        self.root = Tk()
        # ENABLE FOR FRAMLESS WINDOW.
        # self.root.overrideredirect(1)
        
        #
        # Create the layout.
        #
        self.topFrame = Frame(self.root, width=320, height=200,
                           borderwidth=2, relief=RAISED)
        self.topFrame.pack_propagate(False)
        self.topFrame.pack()
        self.bottomFrame = Frame(self.root, width=320, height=150,
                           borderwidth=2, relief=RAISED)
        self.bottomFrame.pack_propagate(False)
        self.bottomFrame.pack()
        
        
        speed = self.getSpeed()
        self.speed = Label(self.topFrame, text=speed, font=("Arial Bold", 25))
        self.speedLabel = Label(self.topFrame, text="KM/h", font=("Arial Bold", 10))

        self.speed.grid(column=0, row=0)
        self.speedLabel.grid(column=1, row=0)


        tach = self.getTach()
        self.tach = Label(self.topFrame, text=tach, font=("Arial Bold", 25))
        self.tachLabel = Label(self.topFrame, text="RPM", font=("Arial Bold", 10))

        self.tach.grid(column=0, row=1)
        self.tachLabel.grid(column=1, row=1)
        
        water = self.getWaterTemp()
        self.waterTemp = Label(self.topFrame, text=water, font=("Arial Bold", 25))
        self.waterTempLabel = Label(self.topFrame, text="water \n temp", font=("Arial Bold", 10))

        self.waterTemp.grid(column=2, row=0)
        self.waterTempLabel.grid(column=3, row=0)
                
        boost = self.getBoost()
        self.boost = Label(self.topFrame, text=boost, font=("Arial Bold", 25))
        self.boostLabel = Label(self.topFrame, text="Boost", font=("Arial Bold", 10))

        self.boost.grid(column=2, row=1)
        self.boostLabel.grid(column=3, row=1)
        
        self.bQuit = Button(self.bottomFrame, text="Quit",
                            command=self.root.quit)
        self.bQuit.pack(pady=20)
        # self.bHello = Button(self.frame, text="Hello",
                            #  command=self.hello)
        # self.bHello.pack(pady=20)



    def hello(self):
        print("Hello")


app = App()
app.root.mainloop()